﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public GameObject player;
    private Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        //offset is a new var. where it keeps track of amont of space between player and camera
        offset = transform.position - player.transform.position;  
    }

    // Update is called once per frame
    // having a late update is making sure that this is the last thing that happens from the update or else
    //it can make weird shakes when moving the player around.
    void LateUpdate()
    {
        // when player starts to move then the position will change which then the offsett will need to be added to stick onto the player.
        transform.position = player.transform.position + offset;
    }
}
