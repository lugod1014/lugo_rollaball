﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        //This is what make the cubes rotate at the 3 float angles x,y, and z then mutiplied by time, delta time
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }
}
