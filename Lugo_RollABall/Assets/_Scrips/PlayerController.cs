﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    //speed and the text of count and win can be used in other scripts
    public float speed;
    public Text countText;
    public Text winText;
    //this specific count int is only beiong used in this script thus making it private.
    Rigidbody rb;
    private int count;

    public GameObject[] floorDestroy;

    // Start is called before the first frame update
    void Start()
    //Here we are setting the count to start at 0 the win text to an empty string and SetCountText Function for layer use.
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //Depending on what player is imputting to move wether vertical or horizontal it is captured
    private void FixedUpdate()
    {
        float movehorizontal = Input.GetAxis("Horizontal");
        float movevirtical = Input.GetAxis("Vertical");
        //movement variable to know how fast it is currently going at that time of the moment of time.
        Vector3 movement = new Vector3(movehorizontal, 0.0f, movevirtical);
        //to then move the movement is then mutiplied by speed to start to make it go faster and faster.
        rb.AddForce(movement * speed);
    }
 
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            //make it so that once picked up it'll disapper
            other.gameObject.SetActive(false);
            //Adds plus one to the count variable we set up before
            count = count + 1;
            //when count becomes 12 the floor will dissapear
            if (count >= 12)
            {
                Destroy(floorDestroy[0]);
            }
            SetCountText();
        }
        //this is so that this item is tagged on the items that have the "BooserTag"
        if (other.gameObject.CompareTag("Booster"))
        {
            //the new velocity once an object goes through the area that has this tag will then be the velocity
            //times 2.
            rb.velocity = rb.velocity * 2;
        }

    }
    //This below is setting the count to get higher when the player collects a cube on screen so then if the count 
    //becomes greater than or equals to 27 then the text "you win" will appear
    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 27)
        {
            winText.text = "You Win";
        }
    }
}
